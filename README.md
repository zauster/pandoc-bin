
# README

This repository contains PKGBUILDs for the pandoc binaries `citeproc` and `crossref`.

## citeproc

```
pandoc -F pandoc-citeproc citeproc_demo.md -o citeproc_demo.pdf
```

## crossref

```
pandoc -F pandoc-crossref crossref_demo.md -o crossref_demo.pdf
```
